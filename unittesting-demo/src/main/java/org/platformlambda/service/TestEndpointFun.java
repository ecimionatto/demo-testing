package org.platformlambda.service;

import com.google.common.collect.ImmutableMap;
import org.platformlambda.MainApp;
import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.LambdaFunction;
import org.platformlambda.core.system.PostOffice;

import java.util.Map;

public class TestEndpointFun implements LambdaFunction {

    private static final String PATH_PREFIX = "/api/test";
    private static final String ID = "id";

    @Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws Exception {

        AsyncHttpRequest request = new AsyncHttpRequest(body);
        String uri = request.getUrl();

        if (uri == null) {
            throw new IllegalArgumentException("Invalid request");
        }
        if (!"GET".equals(request.getMethod())) {
            throw new AppException(405, "Expect: GET, Actual: " + request.getMethod());
        }
        if (!uri.startsWith(PATH_PREFIX)) {
            throw new IllegalArgumentException("URI must start with " + PATH_PREFIX);
        }

        String id = request.getPathParameter(ID);

        //persistence service
        PostOffice.getInstance().send(
                MainApp.SIMPLE_EXAMPLE,
                ImmutableMap.of("businesslogic", true));

        return new EventEnvelope()
                .setBody(ImmutableMap.of("id", id))
                .setStatus(200);
    }

}
