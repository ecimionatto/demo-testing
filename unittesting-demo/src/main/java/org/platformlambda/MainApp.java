package org.platformlambda;

import org.platformlambda.core.annotations.MainApplication;
import org.platformlambda.core.models.EntryPoint;
import org.platformlambda.core.system.Platform;
import org.platformlambda.rest.RestServer;
import org.platformlambda.service.SimpleService;
import org.platformlambda.service.SyncService;
import org.platformlambda.service.TestEndpointFun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@MainApplication
public class MainApp implements EntryPoint {
    private static final Logger log = LoggerFactory.getLogger(MainApp.class);
    public static final String SIMPLE_EXAMPLE = "simple.service";
    public static final String SYNC_SERVICE = "sync.service";
    public static final String TEST_ENDPOINT_FUN = "test.endpoint.fun";

    public static void main(String[] args) {
        RestServer.main(args);
    }

    @Override
    public void start(String[] args) throws Exception {

        Platform platform = Platform.getInstance();

        platform.register(SIMPLE_EXAMPLE, new SimpleService(), 20);
        platform.register(SYNC_SERVICE, new SyncService(), 20);
        platform.register(TEST_ENDPOINT_FUN, new TestEndpointFun(), 20);

        platform.connectToCloud();

        log.info("Application started");
    }

}
