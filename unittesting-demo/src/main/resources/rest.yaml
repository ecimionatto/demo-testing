#
# REST automation is the recommended way to create REST endpoints properly
# Note that URL comparison is case insensitive but case sensitivity is preserved
# for the original URL and path parameters
#
rest:

  - service: "test.endpoint.fun"
    methods: ['GET']
    # id = trace_id
    url: "/api/test/{id}"
    timeout: 12
    cors: cors_1
    headers: header_1

  - service: "test.endpoint.fun"
    methods: ['GET']
    # search for all transactions in a time period
    url: "/api/test?time=end_time&duration=specs&from=offset&size=page_size&org.platformlambda.model.service=route_name&path=method_n_uri"
    timeout: 12
    cors: cors_1
    headers: header_1

#
# CORS HEADERS for pre-flight (HTTP OPTIONS) and normal responses
#
# Access-Control-Allow-Origin must be "*" or domain name starting with "http://" or "https://"
# The use of wildcard "*" should only be allowed for non-prod environments.
#
# For production, please add the "api.origin" key in the application.properties.
# In this example, the api.origin value will be used to override the value
# in "Access-Control-Allow-Origin".
#
cors:
  - id: cors_1
    # origin is optional. If present, it will replace Access-Control-Allow-Origin value in options and headers
    origin: ${api.origin}
    options:
      - "Access-Control-Allow-Origin: *"
      - "Access-Control-Allow-Methods: GET, DELETE, PUT, POST, OPTIONS"
      - "Access-Control-Allow-Headers: Origin, Authorization, X-Session-Id, Accept, Content-Type, X-Requested-With"
      - "Access-Control-Max-Age: 86400"
    headers:
      - "Access-Control-Allow-Origin: *"
      - "Access-Control-Allow-Methods: GET, DELETE, PUT, POST, OPTIONS"
      - "Access-Control-Allow-Headers: Origin, Authorization, X-Session-Id, Accept, Content-Type, X-Requested-With"
      - "Access-Control-Allow-Credentials: true"

#
# add/drop/keep HTTP request and response headers
#
headers:
  - id: header_1
    request:
      #
      # headers to be inserted
      #    add: ["hello-world: nice"]
      #
      # keep and drop are mutually exclusive where keep has precedent over drop
      # i.e. when keep is not empty, it will drop all headers except those to be kept
      # when keep is empty and drop is not, it will drop only the headers in the drop list
      # e.g.
      # keep: ['x-session-id', 'user-agent']
      # drop: ['Upgrade-Insecure-Requests', 'cache-control', 'accept-encoding', 'host', 'connection']
      #
      drop: ['Upgrade-Insecure-Requests', 'cache-control', 'accept-encoding', 'host', 'connection']

    response:
      #
      # the system can filter the response headers set by a target org.platformlambda.model.service
      # but it cannot remove any response headers set by the underlying servlet container.
      # However, you may override non-essential headers using the "add" directive. e.g. the "server" header.
      # i.e. don't touch essential headers such as content-length.
      #
      #     keep: ['only_this_header_and_drop_all']
      #     drop: ['drop_only_these_headers', 'another_drop_header']
      #
      #      add: ["server: mercury 1.12"]
      #
      # You may want to add cache-control to disable browser and CDN caching.
      # add: ["Cache-Control: no-cache, no-store", "Pragma: no-cache", "Expires: Thu, 01 Jan 1970 00:00:00 GMT"]
      #
      add: ["Cache-Control: no-cache, no-store", "Pragma: no-cache", "Expires: Thu, 01 Jan 1970 00:00:00 GMT"]
