package org.platformlambda.model;

import java.util.Date;

public class SyncServiceRequest {

    final private String id;
    final private String code;
    final private Date requestDate;
    final private String description;
    final private Double value;

    public SyncServiceRequest(final String id, final String code, final Date requestDate, final String description, final Double value) {
        this.id = id;
        this.code = code;
        this.requestDate = requestDate;
        this.description = description;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public String getDescription() {
        return description;
    }

    public Double getValue() {
        return value;
    }
}
