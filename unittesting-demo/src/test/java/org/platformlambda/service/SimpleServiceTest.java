package org.platformlambda.service;

import com.google.common.collect.ImmutableMap;
import org.junit.BeforeClass;
import org.junit.Test;
import org.platformlambda.MainApp;
import org.platformlambda.core.system.Platform;
import org.platformlambda.core.system.PostOffice;
import org.platformlambda.model.SyncServiceRequest;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class SimpleServiceTest {

    @BeforeClass
    public static void setup() throws IOException {
        Platform.getInstance().register("sync.service.test",
                (headers, body, instance) -> {
                    SyncServiceRequest request = (SyncServiceRequest) body;
                    if (!request.getCode().equals("error")) {
                        throw new IllegalStateException("test error");
                    }
                    return Optional.of(true);
                }, 1);
    }

    @Test
    public void shouldCallSyncServiceAndReturn() throws Exception {
        Map<String, Boolean> inputMap = ImmutableMap.of("businesslogic", true);
        Optional<Boolean> response = (Optional<Boolean>)
                new SimpleService().handleEvent(ImmutableMap.of(),
                        inputMap,
                        1);
        assertTrue(response.get());
    }

    @Test
    public void shouldIgnoreSyncServiceAndReturn() throws Exception {
        Map<String, Boolean> inputMap = ImmutableMap.of("businesslogic", false);
        Optional<Boolean> response = (Optional<Boolean>)
                new SimpleService().handleEvent(ImmutableMap.of(),
                        inputMap,
                        1);
        assertTrue(!response.isPresent());
    }

}