package org.platformlambda.service;

import com.google.common.collect.ImmutableMap;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.system.Platform;

import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertThat;

public class TestEndpointFunTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @SuppressWarnings("unchecked")
    @Test
    public void shouldGetRequestWithIDAndCallSimpleServiceWIthID() throws Exception {
        Platform.getInstance().register("simple.service.test", (headers, body, instance) -> {
            Map<String, Object> objectMap = (Map<String, Object>) body;
            if (!objectMap.get("id").equals("1")) {
                throw new IllegalStateException("not a match");
            }
            return "true";
        }, 1);

        //when(service.cll()).thenReturn("true");
        //verify(service).call(map);

        Map<String, ImmutableMap<String, String>> params
                = ImmutableMap.of("path", ImmutableMap.of("id", "1"));

        AsyncHttpRequest asyncHttpRequest = new AsyncHttpRequest()
                .setMethod("GET").setPathParameter("id", "1").setUrl("/api/test/1");

        EventEnvelope envelope = (EventEnvelope)
                new TestEndpointFun().handleEvent(
                        ImmutableMap.of(),
                        asyncHttpRequest.toMap(),
                        1);

        Map<String, Object> envelopeBody = (Map<String, Object>) envelope.getBody();

        assertThat("1", CoreMatchers.equalTo(envelopeBody.get("id")));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldThrow405ExceptionWhenPOSTRequest() throws Exception {
        exceptionRule.expect(AppException.class);
        exceptionRule.expectMessage("Expect: GET, Actual: POST");

        AsyncHttpRequest asyncHttpRequest = new AsyncHttpRequest()
                .setMethod("POST").setPathParameter("id", "1").setUrl("/api/test/1");

        new TestEndpointFun()
                .handleEvent(ImmutableMap.of(), asyncHttpRequest.toMap(), 1);

    }

}
