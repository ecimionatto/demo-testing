package org.platformlambda.service;

import com.google.common.collect.ImmutableMap;
import org.platformlambda.MainApp;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.LambdaFunction;
import org.platformlambda.core.system.PostOffice;
import org.platformlambda.model.SyncServiceRequest;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class SimpleService implements LambdaFunction {

    @SuppressWarnings("unchecked")
    @Override
    public Object handleEvent(final Map<String, String> headers, final Object body, final int instance) throws Exception {

        final ImmutableMap<String, Boolean> inputMap = (ImmutableMap<String, Boolean>) body;

        if (!inputMap.get("businesslogic")) {
            return Optional.empty();
        }

        SyncServiceRequest syncServiceRequest = new SyncServiceRequest(
                UUID.randomUUID().toString(),
                "code1",
                new Date(),
                "sync call description",
                1213.12);

        EventEnvelope request = PostOffice.getInstance()
                .request(MainApp.SYNC_SERVICE, 1000,
                        syncServiceRequest);

        return request.getBody();
    }
}
